# README della versione windows di CercaDivisione

---

## Origine del progetto.

Il programma è stato creato con l’intento di sperimentare LiveCode, in particolare per la gestione di database MySQL o SqLite e conseguente presentazione dei dati ottenuti dalle query richieste nell’ambiente grafico nativo.
 
In questo programma il motore database usato è stato SqL ite: dovendo essere distribuito su varie piattaforme e, vista la piccola dimensione del DB e la quasi totale immutabilità del suo contenuto, ho deciso per una versione con file locale. 

## Sviluppo sorgente

Ho iniziato con lo sviluppo per la versione Mac OSX, e non ho trovato particolari difficoltà nel creare il sorgente ed ottenere un eseguibile funzionante praticamente da subito. 

Il programma apre una singola finestra in cui e già tutto pronto per l’interrogazione. Si puoi usare le seguenti cifre per l’interrogazione: 

- 0 si ottiene una lista totale dei codice e relative descrizioni dei paesi 
- numero compreso tra 1 e 346 per ottenere la descrizione specifica del paese indicizzato con la cifra digitata. 
- Nel caso si esca da questo limite, compare un pop-up che segnala quasi siano i valori ammessi nel campo di ricerca. Il pop-up ha un solo tasto OK cliccato il quale, ti torna alla situazione di avvio, ossia richiesta codice paese e griglia vuota.

In fondo, ovviamente, c’è un pulsante **Esci** che chiude il database correttamente, prima di sganciare la finestra dall’ambiente grafico e pulire la memoria, restituendo poi il controllo all’utente.

## Note 
Problemi in versione Windows [^Il problema si è presentato quando ho cercato di ottenere gli stessi risultati, dallo stesso sorgente, in Windows, ma un rapporto, approfondito, delle problematiche sono presenti nel file README.md della corrispondete cartella della versione Windows.]

Cartella SqLite [^Esiste una cartella SqLite che contiene il file originale di testa da cui ho creato il database SqLite, un file di creazione della struttura del DB sia in formato dump di SqLite (.MTQS) che in formato SQL Ansi (.sql).]




---

#### Ultima revisione file: 24/03/2021

