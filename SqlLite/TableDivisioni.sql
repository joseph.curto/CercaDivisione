drop table if exists Divisioni;
create table Divisioni
(
	NumDiv Integer Not Null Default '0',
	NomeDiv VarChar Not Null
);
create unique index if not exists NumDivKey on divisioni(NumDiv);
create index if not exists NomeDivKey on divisioni(NomeDiv);

